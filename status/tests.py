# django & python
from django.test import TestCase, Client
from django.http.request import HttpRequest
import time

# selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# apps
from .models import StatusModel
from . import views


# Create your tests here.
class StatusTest(TestCase):
    # Apakah localhost:8000 bisa diakses?
    def test_url_halaman_status_exists(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    # Apakah StatusModel bisa menerima input status
    # dan masuk ke database?
    def test_models_bekerja_dengan_baik(self):
        StatusModel.objects.create(status="mencoba unit test")
        jumlah_status = StatusModel.objects.all().count()
        self.assertEqual(jumlah_status, 1)

    # Apakah StatusModel di halaman Admin
    # tertulis sebagai inputan statusnya? ("mencoba unit test")
    def test_string_status_di_admin(self):
        StatusModel.objects.create(status="mencoba unit test")
        status_object = StatusModel.objects.get(status="mencoba unit test")
        status_object_string = status_object.__str__()
        self.assertEqual("mencoba unit test", status_object_string)

    # Apakah views terima()
    # bisa berjalan dengan benar?
    def test_apakah_terima_views_bisa_masukin_inputan_ke_html(self):
        responsepos = Client().post('/terima/', {'isi_status' : 'mencoba unit test'})
        responseget = Client().get('/')
        html_response = responseget.content.decode('utf8')
        self.assertIn('mencoba unit test', html_response)

    
class FunctionalTest(TestCase):
    # Aktivasi browser
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    # Tutup browser, hapus data percobaan dari models
    # lalu tunggu, baru tutup
    def tearDown(self):
        StatusModel.objects.all().delete()
        # self.browser.implicitly_wait(2)
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_functional_bisa_masukkan_status_dan_statusnya_ditampilkan_di_tabel(self):
        # Willy memasukkan URL dari website yang dibuat Doan.
        self.browser.get('http://localhost:8000')

        # Yang pertama kali Willy lihat adalah judul dari
        # websitenya (element <title>), yaitu status palsu.
        # time.sleep(2)
        self.assertIn('status palsu', self.browser.title)

        # Willy melihat ada box untuk input status.
        input_status = self.browser.find_element_by_name('isi_status')

        # Willy pun memasukkan status dia sekarang,
        # dan mengklik tombol kirim status.
        input_status.send_keys('Coba Coba')
        # time.sleep(2)
        input_status.submit()
        time.sleep(1)
        
        # Status yang ditulis Willy pun
        # masuk ke tabel, beserta jam kapan status
        # tersebut dibuat. yeayyyy
        self.assertIn('Coba Coba', self.browser.page_source)

        # Willy pun berpikir dalam hati, 
        # "waah, Doan hebat banget ya!! mau deh jadi kayak doan".
        # Setelah itu pun, ia menutup browsernya
        # untuk belajar TDD Django agar dia bisa
        # djago seperti doan. selesai