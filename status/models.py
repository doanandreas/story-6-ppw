from django.db import models

# Create your models here.
class StatusModel(models.Model):
    status = models.CharField(max_length=300)
    waktu_submit = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return self.status