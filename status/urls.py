from django.urls import path
from . import views

app_name = 'status'

urlpatterns = [
    path('', views.index, name='homepage'),
    path('terima/', views.terima, name='terima'),
]
