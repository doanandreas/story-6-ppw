from django.shortcuts import render
from .models import StatusModel
from .forms import StatusForm

from datetime import datetime
import pytz

# Create your views here.
def index(request):
    status_form = StatusForm()
    status_obj = StatusModel.objects.all()
    argument = {
        'status_form': status_form,
        'status_obj' : status_obj,
    }
    return render(request, 'index.html', argument)

def terima(request):
    # waktu saat submit
    datetime_now = datetime.now()

    status_form = StatusForm(request.POST)

    # ambil status dr isian
    isian_status = request.POST['isi_status']

    # buat objek StatusModel
    temp = StatusModel(
        status = isian_status,
        waktu_submit = datetime_now,
    )

    # simpen
    temp.save()

    status_obj = StatusModel.objects.all()

    # tampilin halamannya
    argument = {
        'status_form': status_form,
        'status_obj' : status_obj,
    }
    return render(request, 'index.html', argument)


