from selenium import webdriver
import unittest, time

class VisitorTest(unittest.TestCase):
    def setUp(self):
        # Khintil ingin mencoba web yang baru dibuat
        # oleh Doan. Ia pun membuka Firefox di laptop-nya.
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()
    
    def test_bisa_masukkan_status_dan_tampil_di_tabel(self):
        # Khintil memasukkan URL dari website yang dibuat Doan.
        self.browser.get('http://localhost:8000')
        time.sleep(5)

        # Yang pertama kali Khintil lihat adalah judul dari
        # websitenya (<title>), yaitu (insert title here).
        self.assertIn('status palsu', self.browser.title)
        self.fail('===== Test Selesai! =====')

        time.sleep(5)
        # Setelah beberapa detik menunggu (Khintil tinggal di
        # zimbabwe makanya internet lemot), akhirnya webnya
        # muncul. Heading yang ia lihat adalah (insert <h1> here).

        # Khintil melihat ada box untuk input status.

        # Khintil pun memasukkan status dia sekarang,
        # dan mengklik tombol kirim status.

        # Status yang ditulis Khintil pun
        # masuk ke tabel, beserta jam kapan status
        # tersebut dibuat. yeayyyy

        # Khintil pun berpikir dalam hati, 
        # "waah, Doan hebat banget ya!! mau deh jadi kayak doan".
        # Setelah itu pun, ia menutup browsernya
        # untuk belajar TDD Django agar dia bisa
        # djago seperti doan. selesai

if __name__ == '__main__':  
    unittest.main(warnings='ignore')